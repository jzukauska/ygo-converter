package application;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Vector;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;

public class GuiController implements Initializable {
	// public static final String ydkPath = "./deck";
	public static final String ydkPath = "D:\\ygopro2 english-Oct-3-2017\\deck";
	public static final String databasepath = "D:\\ygopro2 english-Oct-3-2017\\cdb";
	private static final String toggleString = ".cdb";

	private List<String> ydkAbsolutePaths = new ArrayList<String>();

	Vector<String> v = null;

	String loadedydk = null;

	@FXML
	private TextArea textOutput;

	@FXML
	private Button tcgPage;

	@FXML
	public ListView<String> lister;

	private Connection connect(String str) {
		// SQLite connection string
		String url = "jdbc:sqlite:" + str;
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return conn;
	}

	public void convertIt(int ydkIndex) throws NumberFormatException, SQLException {
		int grandTotal = 0;
		// Vector<String> v = this.v;
		// String ydk = this.loadedydk;

		LinkedHashMap<String, Card> cardList = new LinkedHashMap<String, Card>();

		Vector<String> cardNumbers = new Vector<>();

		try (BufferedReader br = new BufferedReader(new FileReader(ydkAbsolutePaths.get(ydkIndex )))) {

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {

				// if (Integer.parseInt(sCurrentLine) ) {
				//
				try {
					Integer.parseInt(sCurrentLine);
					cardNumbers.add(sCurrentLine);
					System.out.println(Integer.parseInt(sCurrentLine));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
				}
				// }

			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		for (Iterator<String> iterator = cardNumbers.iterator(); iterator.hasNext();) {
			String string = iterator.next();
			String cardName = null;
			// System.out.println(string);

			if (cardList.containsKey(string)) {
				Card card = cardList.get(string);
				card.setQuantity(card.getQuantity() + 1);
			} else {

				String tempName = searchDatabase(Integer.parseInt(string));
				cardList.put(string, new Card(1, tempName, string));

			}
			grandTotal++;
		}

		// System.out.println(cardList);
		System.out.println("Total Cards in deck: " + grandTotal);

		textOutput.setText("");
		for (Entry<String, Card> pair : cardList.entrySet()) {
			// iterate over the pairs
			Card card = pair.getValue();

			textOutput.setText(textOutput.getText() + card.formattedOutput());
		}

	}

	public void getPaths() {

		String basePath = databasepath;

		Vector<String> v = new Vector<>();
		v = listFiles(basePath);

		v.add(listFiles(basePath + "\\English").elementAt(0));

		for (Iterator<String> iterator = v.iterator(); iterator.hasNext();) {
			String string = iterator.next();
			System.out.println(string);
		}

		this.v = v;

	}

	@FXML
	public void handleMouseClick(MouseEvent arg0) throws NumberFormatException, SQLException {
		System.out.println("clicked on " + lister.getSelectionModel().getSelectedIndex());
		convertIt(lister.getSelectionModel().getSelectedIndex());

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		populateYDKPaths();
		getPaths();
	}

	public Vector<String> listFiles(String directoryName) {
		Vector<String> list = new Vector<String>();
		File directory = new File(directoryName);
		// get all the files from a directory
		File[] fList = directory.listFiles();
		for (File file : fList) {
			if (file.isFile()) {
				if (file.getName().endsWith(toggleString)) {
					list.add(file.getAbsolutePath());
					// + "\\" + file.getName()
				}

			}
		}
		return list;
	}

	public void openTheWeb() throws IOException, URISyntaxException {

		if (Desktop.isDesktopSupported()) {
			Desktop.getDesktop().browse(new URI("https://store.tcgplayer.com/massentry"));
		}
	}

	private void populateYDKPaths() {
		String directory = ydkPath;
		File[] files = new File(directory).listFiles();
		List<String> deckNames = new ArrayList<String>();
		ydkAbsolutePaths.clear();

		for (File file : files) {
			if (file.isFile() && file.getName().endsWith(".ydk")) {
				deckNames.add(file.getName());
				;
				ydkAbsolutePaths.add(file.getAbsolutePath());
				System.out.println(file.getAbsolutePath());
			}
		}

		ObservableList<String> names = FXCollections.observableArrayList(deckNames);
		lister.setItems(names);

	}

	public String searchDatabase(int i) {
		boolean found = false;

		for (Iterator iterator = v.iterator(); iterator.hasNext() && !found;) {
			String string = (String) iterator.next();
			Connection conn = connect(string);
			Statement stmt = null;
			try {
				stmt = conn.createStatement();
			} catch (SQLException e2) {
				// TODO Auto-generated catch block
				System.out.println("Databases blew up");
			}
			ResultSet rs = null;
			try {
				rs = stmt.executeQuery("SELECT name FROM texts WHERE id = " + i);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				System.out.println("Didn't find anything useful in the database");
			}

			try {
				if (rs.getString(1) != null) {
					return rs.getString(1);
				}
			} catch (SQLException e) {
				System.out.println("Couldn't find a card id = : " + i);

			}
		}

		return null;

	}
}
