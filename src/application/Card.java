package application;

public class Card {
	private int quantity;
	private String name;
	private String id;
	public Card(int quantity, String name, String id) {
		super();
		this.quantity = quantity;
		this.name = name;
		this.id = id;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "Card [quantity=" + quantity + ", name=" + name + ", id=" + id + "] \n";
	}
	public String formattedOutput() {
		return quantity + " " + name + "\n";
		
		
	}

}
